#include "../lib/buffer.h"

#ifndef genHTML_h
#define genHTML_h

int genHTML(char dirI[], char cssI[]);
int genCode(char fileI[], struct buf *ob);
void getRelatives(char* dirI, int slideNum, int hasPositions[]);
int writeFile(struct buf *html, int slideNum, char cssI[], int hasPos[]);

#define READ_UNIT 1024
#define OUTPUT_UNIT 64
#define OUTDIR "output"

#endif
