SDIR := src
SRCS := $(wildcard $(SDIR)/*.c)

ODIR := src/obj
OBJS := $(SRCS:$(SDIR)/%.c=$(ODIR)/%.o)

DDIR := lib
DEPS := $(wildcard $(DDIR)/*.c)

LIBS := -lm

HDIR := headers
HDRS := $(wildcard $(HDIR)/*.h)

CC := gcc
CFLAGS := -g -Wall

TARGET := quickref

default: $(TARGET)

$(ODIR)/%.o: $(SDIR)/%.c
	@echo Compiling Object: $@\n
	$(CC) $(CFLAGS) -c -o $@ $<

$(TARGET): $(OBJS) $(DEPS)
	@echo Compiling with Objects: $(OBJS)\n
	$(CC) $(CFLAGS) -o $@ $^ $(LIBS)

clean:
	$(RM) $(OBJS) $(TARGET) output/* core

.PHONY: default clean
