#include "../headers/genHTML.h"
#include "../lib/buffer.h"
#include "../lib/markdown.h"
#include "../lib/html.h"
#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include <unistd.h>
#include <math.h>

int genHTML(char dirI[], char cssI[]) {
    
    //Copy over the CSS File to index.css
    FILE* cssF = fopen(cssI,"r");
    char* outCN = malloc(0);
    strcpy(outCN,OUTDIR);
    strcat(outCN,"/index.css");
    printf(outCN, "Outfile: %s\n");
    printf("\n");
    FILE* outC = fopen(outCN,"w");
    free(outCN);

    char chr = fgetc(cssF);
    while (chr != -1)
    {
        fputc(chr,outC);
        chr = fgetc(cssF);
    }
    
    fclose(outC);
    fclose(cssF);
    
    printf("Loaded CSS file\n");

    DIR *dir = opendir(dirI); //Create a dir object

    struct dirent *ent = readdir(dir); //Get the first entry
    while (ent) //While entry exists
    {
        int isReg = ent->d_type == DT_REG; //If file type is DT_REG
        int isMD;
        
        char* e = strrchr(ent->d_name,'.'); //Get the file extention
        if (!e) //If it doesn't have a file extention
        {
            isMD = 0; //it's not markdown
        }
        else if (isReg)//If it does and it's not a directory
        {
            if (!strcmp(".md",e)) //If the extention is a md file
            {
                isMD = 1; //Then set the variable that it's markdown
            }
            else
            {
                printf("Did not render for file: %s, file is not a .md\n",ent->d_name); //Whoops!
                ent = readdir(dir); //Move on to next file
                continue; //Continue Loop
            }
        }
        
        if (isReg && isMD) //If it's both a file and markdown
        {
            int slideNum = -1; //Get the number of the slide
            sscanf(ent->d_name,"%i.md",&slideNum); //The filename is [number].md
            if (slideNum == -1) //If the file is not a number
            {
                printf("Did not render for file: %s, filename not a number\n",ent->d_name);
                ent = readdir(dir); //Move on to next file
                continue;
            }

            char* fullName = malloc(0); //Full name is
            strcpy(fullName,dirI); //Directory
            strcat(fullName,"/"); //Plus "/"
            strcat(fullName,ent->d_name); //Plus filename

            struct buf *html; //Create a buffer to store completed HTML
            html = bufnew(READ_UNIT); //Allocate memory to it
            int result = genCode(fullName, html);  //Put HTML in buffer
            if (result) //If it fails
            {
                fprintf(stderr,"Unable to write html from file: %s\n",ent->d_name); //Write to stderr
                return EXIT_FAILURE; //return failed
            }
            int hasPositions[2] = {0,0}; //Int array
            getRelatives(dirI,slideNum,hasPositions); //Gets relative pages
            result = writeFile(html, slideNum, cssI, hasPositions); //Writes html to file and adds trimmings
            free(fullName);
            if (result) //If it fails
            {
                fprintf(stderr,"Unable to write html to file: %s\n",ent->d_name);
                return EXIT_FAILURE;
            }
        }

        ent = readdir(dir); //Get next directory entry
    }
    return EXIT_SUCCESS;
}

void getRelatives(char* dirI, int slideNum, int hasPositions[])
{
    char otherFile[strlen(dirI)]; //Name of the other (potential) MD file
    strcpy(otherFile,dirI);
    
    int numSize = ((int) log10(slideNum)) + 1;
    char numAsStr[numSize]; //Find the slide's number minus one
    sprintf(numAsStr,"%i",slideNum-1);

    strcat(otherFile,"/"); //If the files exists, set hasPositions[0] to True
    strcat(otherFile,numAsStr);
    strcat(otherFile,".md");

    if ( access(otherFile,F_OK) != -1) 
    {
        hasPositions[0] = 1;
    }

    sprintf(numAsStr,"%i",slideNum+1);
    
    strcpy(otherFile,dirI);
    strcat(otherFile,"/");
    strcat(otherFile,numAsStr);
    strcat(otherFile,".md");

    if ( access(otherFile,F_OK) != -1)
    {
        hasPositions[1] = 1;
    }
}

int genCode(char* fileI, struct buf *ob)
{
    printf("Reading File: %s\n",fileI); //Logging
    FILE* in = fopen(fileI,"r"); //Creat file pointer
    if (in == NULL)
    {
        return EXIT_FAILURE;
    }
    int ret; //Return Code
    struct buf *ib; //Input buffer
    ib = bufnew(READ_UNIT); //Create a new buffer
    bufgrow(ib,READ_UNIT); //Increase its size
    while ((ret = fread(ib->data + ib->size, 1, ib->asize - ib->size, in)) > 0) { //While its still possible to read, do so
    	ib->size += ret; // Increase the size field
    	bufgrow(ib, ib->size + READ_UNIT); //Increase its size
    }
    struct sd_callbacks callbacks; //Create callback struct
    struct html_renderopt options; //Options struct
    struct sd_markdown *markdown; //Markdown Struct

    sdhtml_renderer(&callbacks, &options, 0); //Call to renderer
    markdown = sd_markdown_new(0, 16, &callbacks, &options); //Parse MD
    sd_markdown_render(ob, ib->data, ib->size, markdown); //Render based on MD
    sd_markdown_free(markdown); //Free the markdown
    bufrelease(ib); //Free the input buffer
    if (ret >= 0)
        printf("HTML generated for: %s\n",fileI);
    return (ret < 0) ? EXIT_FAILURE : EXIT_SUCCESS; //Return codes
}

int writeFile(struct buf* html,int slideNum, char cssI[], int hasPos[]) {
    //Writing HTML to output file
    char* fname = malloc(0); //Directory to output HTML files to, ex: output
    strcpy(fname,OUTDIR);
    char topName[(int) log10(slideNum) + 6]; //Top-level name, ex: index.html, 2.html
    if (slideNum == 1) //If the slide is the first one, call it index.html
        strcpy(topName,"index.html");
    else { //Otherwise, call it #.md 
        sprintf(topName, "%d",slideNum);
        strcat(topName,".html");
    }

    strcat(fname,"/");
    strcat(fname,topName);
    printf("Writing code to %s\n",fname);
    FILE* out = fopen(fname,"w+");

    //Header
    fprintf(out,"<html>\n");
    fprintf(out,"  <head>\n");
    fprintf(out,"    <title>Shared Page %d</title>",slideNum);
    fprintf(out,"    <link rel=\"stylesheet\" type=\"text/css\" href=\"index.css\">");
    fprintf(out,"  </head>\n\n");

    //Body
    fprintf(out,"  <body>\n");
    fprintf(out,"    ");
    for (size_t i = 0; i < html->size; i++)
    {
        int curr = html->data[i];
        if (curr == 10 && i+1 < html->size)
            fprintf(out,"\n    ");
        else
            fwrite(&html->data[i],1,1,out);
    }

    //Footer
    fprintf(out,"    <span class=\"navContainer\">\n");
    if (hasPos[0] == 1)
    {
        if (slideNum == 2)
        {
            fprintf(out,"      <a class=\"navLink\" href=\"index.html\">Previous</a>\n");
        }
        else
        {
            fprintf(out,"      <a class=\"navLink\" id=\"left\" href=\"%d.html\">Previous</a>\n",slideNum-1);   
        }
    }
    if (hasPos[1] == 1)
    {
        fprintf(out,"      <a class=\"navLink\" id=\"right\" href=\"%d.html\">Next</a>\n",slideNum + 1);
    }
    fprintf(out,"    </span>\n");
    fprintf(out,"  </body>\n\n");
    fprintf(out,"</html>");
    
    fclose(out);
    free(fname);
    return 0;
}
