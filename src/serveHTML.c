#include "../lib/mongoose.h"
#include "../headers/serveHTML.h"
#include "../headers/genHTML.h"

static struct mg_serve_http_opts s_http_server_opts;

// Only serve http requests
static void ev_handler(struct mg_connection *nc, int ev, void *p) {
    if (ev == MG_EV_HTTP_REQUEST)
        mg_serve_http(nc, (struct http_message *) p, s_http_server_opts);
}

int startServer() {
    struct mg_mgr mgr;        //Create Connection Manager
    struct mg_connection *nc; //Create Connection

    mg_mgr_init(&mgr, NULL);  //Initialize manager at pointer
    printf("\nStarting web server on port %s\n", s_http_port); //Log
    nc = mg_bind(&mgr, s_http_port, ev_handler); //Bind any event on given port to handler
    if (nc == NULL) { //Error Logging
        fprintf(stderr,"Failed to create listener\n");
        return 1;
    }

    // Set up HTTP server parameters
    mg_set_protocol_http_websocket(nc);

    s_http_server_opts.document_root = "./output";  // Serve output directory
    printf("Hosting server at %s directory\n",s_http_server_opts.document_root);

    for (;;)
        mg_mgr_poll(&mgr, 1000); //Poll for events on that port

    mg_mgr_free(&mgr); //Free the memory

    return 0;
}
