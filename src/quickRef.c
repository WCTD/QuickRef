#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <dirent.h>
#include <errno.h>
#include "../headers/quickRef.h"
#include "../headers/genHTML.h"
#include "../headers/serveHTML.h"

void help() {
    printf(
    "\nquickref (directory) [options]\n\n"
    "Directory: \nThe location of the files to be compiled.\n"
    "The files should be named 1.md, 2.md, 3.md in the order "
    "they're presented in\n\n"
    "Options:\n"
    "   -h / --help    Display Help\n"
    "   -c / --css     Define a CSS file\n");
}

int main(int argc, char *argv[]) {

    char cssFile[255];
    strcpy(cssFile, defaultFile);

    if (argc == 1) /* If no arguments are given, send back help*/
    {
        fprintf(stderr,"No directory given\n");
        help();
        return EXIT_FAILURE;
    }
    
    for (int i = 1; i < argc; i++)
    {
        if (strcmp(argv[i],"-h") == 0 || strcmp(argv[i],"--help") == 0) {
            printf("Program Help:\n");
            help();
            return EXIT_SUCCESS;
        } else if (strcmp(argv[i],"-c") == 0 || strcmp(argv[i],"--css") == 0) {
            if (argc >= i+2) {
                if (access(argv[i+1],R_OK) == 0) {
                    strcpy(cssFile,argv[i+1]);
                    i++;
                    continue;
                } else {
                    fprintf(stderr,"CSS file given was not readable\n");
                    return EXIT_FAILURE;
                }
            } else {
                fprintf(stderr,"No CSS file given with -c or --css option\n");
                help();
                return EXIT_FAILURE;
            }
        }
    }
   
    char dirStr[255]; 
    strncpy(dirStr, argv[1], 254);

    DIR* dir = opendir(dirStr);

    if (ENOENT == errno || !dir) {
        fprintf(stderr,"Invalid Directory: %s\n",dirStr);
        return EXIT_FAILURE;
    }

    puts("Found directory\n");

    puts("Generating HTML");

    int result = genHTML(dirStr,cssFile);

    if (result != 0)
        return EXIT_FAILURE;

    startServer();
}
